//
//  CoreLocationController.swift
//  weatheralerts
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

//import Foundation
import CoreLocation

class CoreLocationController : NSObject, CLLocationManagerDelegate {
  
    var locationManager:CLLocationManager = CLLocationManager()
    var userLocation: String?
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.distanceFilter  = 3000                         // Must move at least 3km
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer // Accurate within a kilometer
        self.locationManager.requestAlwaysAuthorization()
    }
  
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        println("didChangeAuthorizationStatus")
    
        switch status {
            case .NotDetermined:
                println(".NotDetermined")
                break
      
        case .Authorized:
            println(".Authorized")
            self.locationManager.startUpdatingLocation()
            break
      
        case .Denied:
            println(".Denied")
            break
      
        default:
            println("Unhandled authorization status")
            break
    
        }
    }
  
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    
        let location = locations.last as CLLocation
    
        println("didUpdateLocations:  \(location.coordinate.latitude), \(location.coordinate.longitude)")
    
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, e) -> Void in
            if let error = e {
                println("Error:  \(e.localizedDescription)")
            } else {
                let placemark = placemarks.last as CLPlacemark
        
                let userInfo = [
                    "city":     placemark.locality,
                    "country":  placemark.country
                ]
                NSNotificationCenter.defaultCenter().postNotificationName("LOCATION_AVAILABLE", object: nil, userInfo: userInfo)

            }
        })
    }
}
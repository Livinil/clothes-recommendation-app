import UIKit

class SignInViewController: UIViewController {
   
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.customBlue()
        self.signInButton.backgroundColor = UIColor.darkGrayColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func signInTapped(sender: UIButton) {
        var username:String = usernameTextField.text
        var password:String = passwordTextField.text
        
        if ( username == "" || password == "" ) {
            self.signInErrorAlertView("Please enter Username and Password")
        } else {
            let request = ApiRequest()
            let authSuccess = request.auth(username, password: password)
            if  authSuccess {
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                self.signInErrorAlertView(request.errorMessage!)
            }
        }
    }
    
    func signInErrorAlertView(errorMessage: String) {
        var errorAlertView:UIAlertView = UIAlertView()
        errorAlertView.title = "Sign in Failed!"
        errorAlertView.message = errorMessage
        errorAlertView.delegate = self
        errorAlertView.addButtonWithTitle("OK")
        errorAlertView.show()
    }
}


import UIKit

class GuestTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.translucent = false
        self.tabBar.barTintColor = UIColor.customDarkGrey()
        self.tabBar.tintColor = UIColor.whiteColor()
    }
}

import UIKit

class PreferencesTableViewController: UITableViewController {
    
    
    @IBOutlet weak var milesCell: UITableViewCell!
    @IBOutlet weak var kmCell: UITableViewCell!
    @IBOutlet weak var fahrenheitCell: UITableViewCell!
    @IBOutlet weak var celsiusCell: UITableViewCell!
    @IBOutlet weak var hotCell: UITableViewCell!
    @IBOutlet weak var moderateCell: UITableViewCell!
    @IBOutlet weak var coldCell: UITableViewCell!
    @IBOutlet weak var moderateDetailLabel: UILabel!
    @IBOutlet weak var maleCell: UITableViewCell!
    @IBOutlet weak var femaleCell: UITableViewCell!
    
    var preferences: Preferences = Preferences()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var temperatureUnitSession: String = ""
        var speedUnitSession: String = ""
        var minHotTemp: Float?
        var maxColdTemp: Float?
        var genre: String = ""
        
        moderateCell.userInteractionEnabled = false
        
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if(session.valueForKey("temperatureUnit") != nil) {
            temperatureUnitSession = session.valueForKey("temperatureUnit") as String
        }
        if(session.valueForKey("speedUnit") != nil) {
            speedUnitSession = session.valueForKey("speedUnit") as String
        }
        if(session.valueForKey("minHotTemp") != nil) {
            minHotTemp = session.floatForKey("minHotTemp") as Float?
        }
        if(session.valueForKey("maxColdTemp") != nil) {
            maxColdTemp = session.floatForKey("maxColdTemp") as Float?
        }
        if(session.valueForKey("genre") != nil) {
            genre = session.valueForKey("genre") as String
        }
        moderateDetailLabel.text = "Between \(maxColdTemp!) and \(minHotTemp!)"
        
        if(temperatureUnitSession == "c") {
            self.fahrenheitCell.accessoryType = UITableViewCellAccessoryType.None
            self.celsiusCell.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        } else if(temperatureUnitSession == "f") {
            self.fahrenheitCell.accessoryType = UITableViewCellAccessoryType.Checkmark
            self.celsiusCell.accessoryType = UITableViewCellAccessoryType.None
        }
        if(speedUnitSession == "m") {
            self.milesCell.accessoryType = UITableViewCellAccessoryType.Checkmark
            self.kmCell.accessoryType = UITableViewCellAccessoryType.None
            
        } else if(speedUnitSession == "f") {
            self.milesCell.accessoryType = UITableViewCellAccessoryType.None
            self.kmCell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        if(genre == "male") {
            self.maleCell.accessoryType = UITableViewCellAccessoryType.Checkmark
            self.femaleCell.accessoryType = UITableViewCellAccessoryType.None
        } else if(genre == "female") {
            self.femaleCell.accessoryType = UITableViewCellAccessoryType.Checkmark
            self.maleCell.accessoryType = UITableViewCellAccessoryType.None
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        switch (indexPath.section)
        {
        case 0:
            switch (indexPath.row)
            {
                case 0:
                    self.milesCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.kmCell.accessoryType = UITableViewCellAccessoryType.None
                    preferences.speedUnit = "m"
                    break
                case 1:
                    self.kmCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.milesCell.accessoryType = UITableViewCellAccessoryType.None
                    preferences.speedUnit = "k"
                    break
                default:
                    break
            }
            break;
        case 1:
            switch (indexPath.row)
            {
                case 0:
                    self.fahrenheitCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.celsiusCell.accessoryType = UITableViewCellAccessoryType.None
                    preferences.temperatureUnit = "f"
                    moderateDetailLabel.text = "Between 50 and 68"
                    preferences.maxColdTemp = 50
                    preferences.minHotTemp = 68
                    break
                case 1:
                    self.celsiusCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.fahrenheitCell.accessoryType = UITableViewCellAccessoryType.None
                    preferences.temperatureUnit = "c"
                    moderateDetailLabel.text = "Between 10 and 20"
                    preferences.maxColdTemp = 10
                    preferences.minHotTemp = 20
                    break
                default:
                    break
            }
            break;
        case 2:
            switch (indexPath.row)
            {
            case 0:
                var inputTextField: UITextField?
                var alert = UIAlertController(title: "Edit temperature", message: "Hot above", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                    textField.text = "\(self.preferences.minHotTemp!)"
                    inputTextField = textField
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    let resultString = inputTextField?.text
                    let numberFormatter = NSNumberFormatter()
                    let resultFloat = numberFormatter.numberFromString(resultString!)?.floatValue
                    let maxColdTemp = self.preferences.maxColdTemp
                    if(resultFloat > maxColdTemp) {
                        self.moderateDetailLabel.text = "Between \(maxColdTemp!) and \(resultFloat!)"
                        self.preferences.minHotTemp = resultFloat!
                    } else {
                        self.errorAlertView("This temperature must be higher than the temperature value for cold.")
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                break
            case 2:
                var inputTextField: UITextField?
                var alert = UIAlertController(title: "Edit temperature", message: "Cold below", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                    textField.text = "\(self.preferences.maxColdTemp!)"
                    inputTextField = textField
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    let resultString = inputTextField?.text
                    let numberFormatter = NSNumberFormatter()
                    let resultFloat = numberFormatter.numberFromString(resultString!)?.floatValue
                    let minHotTemp = self.preferences.minHotTemp
                    if(resultFloat < minHotTemp) {
                        self.moderateDetailLabel.text = "Between \(resultFloat!) and \(minHotTemp!)"
                        self.preferences.maxColdTemp = resultFloat!
                    } else {
                        self.errorAlertView("This temperature must be lower than the temperature value for hot.")
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                break
            default:
                break
            }
        case 3:
            switch (indexPath.row)
            {
                case 0:
                    self.maleCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.femaleCell.accessoryType = UITableViewCellAccessoryType.None
                    self.preferences.genre = "male"
                    break
                case 1:
                    self.femaleCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    self.maleCell.accessoryType = UITableViewCellAccessoryType.None
                    self.preferences.genre = "female"
                    break
                default:
                    break
            }
        default:
            break
        }
        var parentView = self.parentViewController as PreferencesViewController
        parentView.preferences = self.preferences
    }
    
    func errorAlertView(message: String) {
        var errorAlertView:UIAlertView = UIAlertView()
        errorAlertView.title = "Preferences error"
        errorAlertView.message = message
        errorAlertView.delegate = self
        errorAlertView.addButtonWithTitle("OK")
        errorAlertView.show()
    }
}

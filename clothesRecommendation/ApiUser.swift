import UIKit

class ApiUser: NSObject {
    
    private var _username: String?
    var username: String? {
        get {
            return self._username
        }
        set {
            self._username = newValue
        }
    }
   
}

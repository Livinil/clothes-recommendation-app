import UIKit

class AccountViewController: UIViewController {
    
    var username: AnyObject?
    var password: AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "accountDone") {
            if(self.username == nil && self.password == nil ) {
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                let request = ApiRequest()
                let accountUpdateSuccess = request.account(username as? String, password: password as? String)
                if accountUpdateSuccess {
                    var alertView:UIAlertView = UIAlertView()
                    alertView.title = "Account update Success!"
                    alertView.message = "Your account has been successfully updated. Please sign in again to try your new account access."
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    session.setValue(nil, forKey: "Authorization")
                    session.setValue(nil, forKey: "username")
                    session.synchronize()
                    self.dismissViewControllerAnimated(true, completion: nil)
                } else {
                    self.errorAlertView(request.errorMessage!, errorTitle: "Account update Failed!")
                }
            }
        }
    }
    
    func errorAlertView(errorMessage: String, errorTitle: String) {
        var errorAlertView:UIAlertView = UIAlertView()
        errorAlertView.title = errorTitle
        errorAlertView.message = errorMessage
        errorAlertView.delegate = self
        errorAlertView.addButtonWithTitle("OK")
        errorAlertView.show()
    }
}

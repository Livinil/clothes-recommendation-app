import UIKit

class AccountTableViewController: UITableViewController {
    
    @IBOutlet weak var usernameCell: UITableViewCell!
    @IBOutlet weak var passwordCell: UITableViewCell!
    @IBOutlet weak var usernameLabel: UILabel!
    var username: AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        username = session.valueForKey("username")
        usernameLabel.text = "\(username!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.row)
        {
            case 0:
                var inputTextField: UITextField?
                var alert = UIAlertController(title: "Change username", message: "Please enter the new username", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                    textField.text = "\(self.username!)"
                    inputTextField = textField
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    if (inputTextField?.text != "\(self.username!)" && inputTextField?.text != "") {
                        self.username = inputTextField?.text
                        self.usernameLabel.text = inputTextField?.text
                        var parentView = self.parentViewController as AccountViewController
                        parentView.username = self.username
                    }
                    if(inputTextField?.text == ""){
                        var errorAlertView:UIAlertView = UIAlertView()
                        errorAlertView.title = "Username change failed"
                        errorAlertView.message = "The username field can't be empty"
                        errorAlertView.delegate = self
                        errorAlertView.addButtonWithTitle("OK")
                        errorAlertView.show()
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                break
            case 1:
                var newInputTextField: UITextField?
                var confirmInputTextField: UITextField?
                var alert = UIAlertController(title: "Change password", message: "Please enter the new password", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                    textField.placeholder = "New password"
                    textField.secureTextEntry = true
                    newInputTextField = textField
                })
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                    textField.placeholder = "Confirm password"
                    textField.secureTextEntry = true
                    confirmInputTextField = textField
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    if (newInputTextField?.text != confirmInputTextField?.text) {
                        var errorAlertView:UIAlertView = UIAlertView()
                        errorAlertView.title = "Password change failed"
                        errorAlertView.message = "The two password must be identical"
                        errorAlertView.delegate = self
                        errorAlertView.addButtonWithTitle("OK")
                        errorAlertView.show()
                    }
                    else if(newInputTextField?.text == "" && confirmInputTextField?.text == "") {
                        var errorAlertView:UIAlertView = UIAlertView()
                        errorAlertView.title = "Password change failed"
                        errorAlertView.message = "The fields can't be empty"
                        errorAlertView.delegate = self
                        errorAlertView.addButtonWithTitle("OK")
                        errorAlertView.show()
                    }
                    else {
                        var parentView = self.parentViewController as AccountViewController
                        parentView.password = confirmInputTextField?.text
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                break
            default:
                break
        }
    }

}

import Foundation

class Preferences: NSObject {
    
    private var _speedUnit: String?
    var speedUnit: String? {
        get {
            return self._speedUnit
        }
        set {
            self._speedUnit = newValue
        }
    }
    
    private var _temperatureUnit: String?
    var temperatureUnit: String? {
        get {
            return self._temperatureUnit
        }
        set {
            self._temperatureUnit = newValue
        }
    }
    
    private var _minHotTemp: Float?
    var minHotTemp: Float? {
        get {
            return self._minHotTemp
        }
        set {
            self._minHotTemp = newValue
        }
    }
    
    private var _maxColdTemp: Float?
    var maxColdTemp: Float? {
        get {
            return self._maxColdTemp
        }
        set {
            self._maxColdTemp = newValue
        }
    }
    
    private var _genre: String?
    var genre: String? {
        get {
            return self._genre
        }
        set {
            self._genre = newValue
        }
    }
    
    override init() {
        super.init()
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        self.speedUnit = session.valueForKey("speedUnit") as String?
        self.temperatureUnit = session.valueForKey("temperatureUnit") as String?
        self.minHotTemp = session.floatForKey("minHotTemp") as Float?
        self.maxColdTemp = session.floatForKey("maxColdTemp") as Float?
        self.genre = session.valueForKey("genre") as String?
    }
}
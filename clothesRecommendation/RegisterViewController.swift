import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var password2TextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerButton.backgroundColor = UIColor.darkGrayColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func registerTapped(sender: UIButton) {
        var username:String = usernameTextField.text as String
        var password:String = passwordTextField.text as String
        var confirm_password:String = password2TextField.text as String
        
        if ( username == "" || password == "" ) {
            self.errorAlertView("Please enter Username and Password", errorTitle: "Registration Failed!")
        }
        else if ( password != confirm_password ) {
            self.errorAlertView("Passwords doesn't Match", errorTitle: "Registration Failed!")
        }
        else {
            let request = ApiRequest()
            let registrationSuccess = request.registration(username, password: password)
            if registrationSuccess {
                var alertView:UIAlertView = UIAlertView()
                alertView.title = "Registration Success!"
                alertView.message = "You've been successfully registered as \(username)"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
                let authRequest = ApiRequest()
                let authSuccess = authRequest.auth(username, password: password)
                if  authSuccess {
                    self.dismissViewControllerAnimated(true, completion: nil)
                } else {
                    self.errorAlertView(authRequest.errorMessage!, errorTitle: "Sign in Failed!")
                }
            } else {
                self.errorAlertView(request.errorMessage!, errorTitle: "Registration Failed!")
            }
        }
    }
    
    func errorAlertView(errorMessage: String, errorTitle: String) {
        var errorAlertView:UIAlertView = UIAlertView()
        errorAlertView.title = errorTitle
        errorAlertView.message = errorMessage
        errorAlertView.delegate = self
        errorAlertView.addButtonWithTitle("OK")
        errorAlertView.show()
    }
}


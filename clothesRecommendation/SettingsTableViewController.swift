import UIKit

class SettingsTableViewController: UITableViewController {
    @IBOutlet weak var preferencesCell: UITableViewCell!
    @IBOutlet weak var locationCell: UITableViewCell!
    @IBOutlet weak var accountCell: UITableViewCell!
    @IBOutlet weak var signOutCell: UITableViewCell!
    
    override func prepareForSegue(segue: (UIStoryboardSegue!), sender: AnyObject!) {
        if (segue.identifier == "toPreferences") {
            self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
        }
        if(segue.identifier == "toGuest") {
            var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            session.setValue(nil, forKey: "Authorization")
            session.synchronize()
            println("session out")
            self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}

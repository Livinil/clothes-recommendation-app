import UIKit

class ApiRecommendation: NSObject {
    
    var statusCode: Int?
    var errorMessage: String?
    var weather: APIWeather?
    var clothes: APIClothes?
    
    init(data: NSDictionary, response: NSURLResponse) {
        super.init()
        if let http = response  as?  NSHTTPURLResponse {
            self.statusCode = http.statusCode
            if 200 == http.statusCode {
                var weatherData: Dictionary <String, NSObject> = data["weather"] as Dictionary
                self.setWeather(weatherData)
                var clothesData: Dictionary <String, NSObject> = data["clothes"] as Dictionary
                self.setClothes(clothesData)
            } else {
                self.errorMessage = data.valueForKey("detail") as? String
            }
        }
    }
    
    func setWeather(weatherData: Dictionary<String, NSObject>) {
        self.weather = APIWeather()
        self.weather?.overview = weatherData["description"] as? String
        self.weather?.sunrise = weatherData["sunrise"] as? String
        self.weather?.sunset = weatherData["sunset"] as? String
        
        if let temperature: Dictionary <String, NSObject> = weatherData["temperature"] as? Dictionary {
            self.weather?.temperatureValue = temperature["value"] as? Int
            self.weather?.temperatureSymbol = temperature["unit"]?.valueForKeyPath("symbol") as? String
        }
        
        if let precipitation: Dictionary <String, NSObject> = weatherData["precipitation"] as? Dictionary {
            self.weather?.precipitationValue = precipitation["value"] as? Int
            self.weather?.precipitationSymbol = precipitation["unit"]?.valueForKeyPath("symbol") as? String
        }
        
        if let wind: Dictionary <String, NSObject> = weatherData["wind"] as? Dictionary {
            self.weather?.windValue = wind["value"] as? Int
            self.weather?.windSymbol = wind["unit"]?.valueForKeyPath("symbol") as? String
        }
        
        if let humidity: Dictionary <String, NSObject> = weatherData["humidity"] as? Dictionary {
            self.weather?.humidityValue = humidity["value"] as? Int
            self.weather?.humiditySymbol = humidity["unit"]?.valueForKeyPath("symbol") as? String
        }
    }
    
    func setClothes(clothesData: Dictionary<String, NSObject>) {
        self.clothes = APIClothes()
        if let head: Dictionary <String, NSObject> = clothesData["head"] as? Dictionary {
            self.clothes?.headLabel = head["label"] as? String
            self.clothes?.headImage = head["image"] as? String
        }
        
        if let neck: Dictionary <String, NSObject> = clothesData["neck"] as? Dictionary {
            self.clothes?.neckLabel = neck["label"] as? String
            self.clothes?.neckImage = neck["image"] as? String
        }
        
        if let top: Dictionary <String, NSObject> = clothesData["top"] as? Dictionary {
            self.clothes?.topLabel = top["label"] as? String
            self.clothes?.topImage = top["image"] as? String
        }
        
        if let legs: Dictionary <String, NSObject> = clothesData["legs"] as? Dictionary {
            self.clothes?.legsLabel = legs["label"] as? String
            self.clothes?.legsImage = legs["image"] as? String
        }
        
        if let feet: Dictionary <String, NSObject> = clothesData["feet"] as? Dictionary {
            self.clothes?.feetLabel = feet["label"] as? String
            self.clothes?.feetImage = feet["image"] as? String
        }
        
    }
   
}

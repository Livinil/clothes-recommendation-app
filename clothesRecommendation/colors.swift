import UIKit

extension UIColor
{
    class func customDarkGrey() -> UIColor {
        return UIColor(red: 0.34, green: 0.34, blue: 0.34, alpha: 1.0)
    }
    
    class func customBlue() -> UIColor {
        return UIColor(red: 0, green: 1.88, blue: 2.12, alpha: 1.0)
    }
}
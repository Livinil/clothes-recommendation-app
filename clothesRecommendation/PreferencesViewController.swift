import UIKit

class PreferencesViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    var preferences: Preferences?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = self.cancelButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "preferencesDone") {
            var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            session.setValue(preferences?.speedUnit, forKey: "speedUnit")
            session.setValue(preferences?.temperatureUnit, forKey: "temperatureUnit")
            session.setValue(preferences?.maxColdTemp, forKey: "maxColdTemp")
            session.setValue(preferences?.minHotTemp, forKey: "minHotTemp")
            session.setValue(preferences?.genre, forKey: "genre")
            session.synchronize()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
}

import UIKit

class ApiRequest: NSObject {
    
    // Store an HTTP request that will be used for calling the API
    let httpRequest: NSMutableURLRequest = NSMutableURLRequest()
    
    // Store the bearer if needed
    let bearer: String?
    
    var errorMessage: String?
    
    override init() {
        self.httpRequest.URL = NSURL(string:"http://weather.generation-pc.net")
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        self.bearer = session.valueForKey("Authorization") as? String
    }
    
    func getRecommendation(requestParams: Dictionary<String, String>) -> ApiRecommendation? {
        if let bearer = self.bearer {
            self.httpRequest.URL = self.httpRequest.URL?.URLByAppendingPathComponent("/api/get-recommendation")
            // set bearer to httpRequest
            httpRequest.HTTPMethod = "POST"
            var err: NSError?
            httpRequest.HTTPBody = NSJSONSerialization.dataWithJSONObject(requestParams, options: nil, error: &err)
            httpRequest.setValue(self.bearer!, forHTTPHeaderField: "Authorization")
            httpRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            httpRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            var reponseError: NSError?
            var response: NSURLResponse?
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(httpRequest, returningResponse:&response, error:&reponseError)
            if ( urlData != nil ) {
                var error: NSError?
                if let jsonResult = NSJSONSerialization.JSONObjectWithData(urlData!, options: .MutableLeaves, error: &err) as? NSDictionary {
                    return ApiRecommendation(data: jsonResult, response: response!)
                } else {
                    return nil
                }
            } else {
                return nil
            }

        } else {
            return nil
        }
    }
    
    func registration(username: String, password: String) -> Bool {
        var registrationSuccess: Bool = false
        self.httpRequest.URL = self.httpRequest.URL?.URLByAppendingPathComponent("/api/registration")
        var params: String = String(format: "%@=%@&%@=%@", arguments: ["username", username, "password", password])
        httpRequest.HTTPMethod = "POST"
        httpRequest.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        httpRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        httpRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(httpRequest, returningResponse:&response, error:&reponseError)
        
        if ( urlData != nil ) {
            let res = response as NSHTTPURLResponse;
            var error: NSError?
            if let jsonResult = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary {
                if (res.statusCode == 200) {
                    registrationSuccess = true
                } else {
                    errorMessage = (jsonResult.valueForKey("validation_messages")?.valueForKey("username") as Array)[0] as String
                }
            }
        }
        return registrationSuccess
    }
    
    func account(username: String?, password: String?) -> Bool {
        var accountUpdateSuccess: Bool = false
        self.httpRequest.URL = self.httpRequest.URL?.URLByAppendingPathComponent("/api/account")
        var params: Dictionary<String,String>?
        if(username != nil && password != nil) {
            params = [
                "username": username!,
                "password": password!
            ] as Dictionary
        } else if (username != nil){
            params = [
                "username": username!
            ] as Dictionary
        } else if(password != nil) {
            params = [
                "password": password!
            ] as Dictionary
        }
        httpRequest.HTTPMethod = "POST"
        var err: NSError?
        httpRequest.HTTPBody = NSJSONSerialization.dataWithJSONObject(params!, options: nil, error: &err)
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let bearer: String? = session.valueForKey("Authorization") as? String
        httpRequest.setValue(bearer!, forHTTPHeaderField: "Authorization")
        httpRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        httpRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(httpRequest, returningResponse:&response, error:&reponseError)
        if ( urlData != nil ) {
            let res = response as NSHTTPURLResponse;
            var error: NSError?
            if let jsonData = NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary {
                if (res.statusCode == 200) {
                    accountUpdateSuccess = true
                } else {
                    self.errorMessage = (jsonData.valueForKey("validation_messages")?.valueForKey("username") as Array)[0] as String
                }
            }
        }
        return accountUpdateSuccess
    }
    
    func auth(username: String, password: String) ->Bool {
        var authSuccess: Bool = false
        self.httpRequest.URL = self.httpRequest.URL?.URLByAppendingPathComponent("/auth")
        let params: String = String(format: "%@=%@&%@=%@&%@=%@", arguments: ["grant_type", "password", "username", username, "password", password])
        let loginString = String(format: "%@:%@", "app", "test")
        let loginData: NSData! = loginString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64LoginString = loginData.base64EncodedStringWithOptions(nil)
        httpRequest.HTTPMethod = "POST"
        var err: NSError?
        httpRequest.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        httpRequest.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        httpRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        httpRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(httpRequest, returningResponse:&response, error:&reponseError)
        if ( urlData != nil ) {
            var error: NSError?
            if let jsonResult = NSJSONSerialization.JSONObjectWithData(urlData!, options: .MutableLeaves, error: &err) as? NSDictionary {
                if let http = response  as?  NSHTTPURLResponse {
                    if 200 == http.statusCode {
                        let token: AnyObject? = jsonResult["access_token"]
                        if (jsonResult["access_token"] != nil) {
                            authSuccess = true
                            var success = jsonResult["access_token"] as String
                            var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                            session.setValue("Bearer \(success)", forKey: "Authorization")
                            session.setValue(username, forKey: "username")
                            session.synchronize()
                        }
                    }
                    else {
                        self.errorMessage = jsonResult.valueForKey("detail") as? String
                    }
                } else {
                    self.errorMessage = jsonResult.valueForKey("detail") as? String
                    
                }
            } else {
                self.errorMessage = "An error occured during the connection to the API."
            }
        }
        return authSuccess
    }
    
    func getImageClothes(path: String) -> UIImage? {
        if let image = NSData(contentsOfURL: NSURL(string: "http://weather.generation-pc.net\(path)")!) {
            return UIImage(data: image)!
        } else {
            return nil
        }
    }
}

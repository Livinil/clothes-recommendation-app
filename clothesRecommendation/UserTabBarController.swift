import UIKit

class UserTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.translucent = false
        self.tabBar.barTintColor = UIColor.customDarkGrey()
        self.tabBar.tintColor = UIColor.whiteColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if (prefs.valueForKey("Authorization") == nil) {
            self.performSegueWithIdentifier("goto_auth", sender: self)
        }
        else {
            self.childViewControllers.first?.viewDidLoad()
        }
    }
}

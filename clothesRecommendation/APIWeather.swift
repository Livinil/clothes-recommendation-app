import UIKit

class APIWeather: NSObject {
    
    private var _overview: String?
    var overview: String? {
        get {
            return self._overview
        }
        set {
            self._overview = newValue
        }
    }
    private var _temperatureValue: Int?
    var temperatureValue: Int? {
        get {
            return self._temperatureValue
        }
        set {
            self._temperatureValue = newValue
        }
    }
    private var _temperatureSymbol: String?
    var temperatureSymbol: String? {
        get {
            return self._temperatureSymbol
        }
        set {
            self._temperatureSymbol = newValue
        }
    }
    private var _precipitationValue: Int?
    var precipitationValue: Int? {
        get {
            return self._precipitationValue
        }
        set {
            self._precipitationValue = newValue
        }
    }
    
    private var _precipitationSymbol: String?
    var precipitationSymbol: String? {
        get {
            return self._precipitationSymbol
        }
        set {
            self._precipitationSymbol = newValue
        }
    }
    
    private var _windValue: Int?
    var windValue: Int? {
        get {
            return self._windValue
        }
        set {
            self._windValue = newValue
        }
    }
    
    private var _windSymbol: String?
    var windSymbol: String? {
        get {
            return self._windSymbol
        }
        set {
            self._windSymbol = newValue
        }
    }
    
    private var _humidityValue: Int?
    var humidityValue: Int? {
        get {
            return self._humidityValue
        }
        set {
            self._humidityValue = newValue
        }
    }
    
    private var _humiditySymbol: String?
    var humiditySymbol: String? {
        get {
            return self._humiditySymbol
        }
        set {
            self._humiditySymbol = newValue
        }
    }
    
    private var _sunrise: String?
    var sunrise: String? {
        get {
            return self._sunrise
        }
        set {
            self._sunrise = newValue
        }
    }
    
    private var _sunset: String?
    var sunset: String? {
        get {
            return self._sunset
        }
        set {
            self._sunset = newValue
        }
    }
}

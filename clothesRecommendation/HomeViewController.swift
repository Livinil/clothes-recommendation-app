import UIKit
import CoreLocation

public class HomeViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet weak var titleLabel: UINavigationBar!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var neckLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var legsLabel: UILabel!
    @IBOutlet weak var feetLabel: UILabel!
    
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var neckImage: UIImageView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var legsImage: UIImageView!
    @IBOutlet weak var feetImage: UIImageView!
    
    
    var userInfo: Dictionary<String, String>?
    
    override public func viewDidLoad() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationAvailable:", name: "LOCATION_AVAILABLE", object: nil)
        super.viewDidLoad()
        setDefaultSession()
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if(session.valueForKey("City") != nil && session.valueForKey("Country") != nil) {
            var city: String = session.valueForKey("City") as String
            var country: String = session.valueForKey("Country") as String
            self.cityLabel.text = "The weather today in \(city)"
            if(session.valueForKey("Authorization") != nil) {
                self.displayData()
            }
        }
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationAvailable(notification:NSNotification) -> Void {
        userInfo = notification.userInfo as? Dictionary<String,String>
        let city = userInfo?.values.first as String!
        let country = userInfo?.values.last as String!
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let lastCity: String? = session.valueForKey("City") as? String
        let lastCountry: String? = session.valueForKey("Country") as? String
        session.setValue(city, forKey: "City")
        session.setValue(country, forKey: "Country")
        session.synchronize()
        if(lastCity != nil && city != lastCity! || lastCountry != nil && country != lastCountry!) {
            self.viewDidLoad()
        }
    }
    
    func displayData() {
        let request = ApiRequest()
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let city: String = session.valueForKey("City") as String
        let country: String = session.valueForKey("Country") as String
        let minHotTemp: Float = session.valueForKey("minHotTemp") as Float
        let maxColdTemp: Float = session.valueForKey("maxColdTemp") as Float
        var params =
        [
            "city": "\(city),\(country)",
            "temperature_unit": session.valueForKey("temperatureUnit") as String,
            "distance_unit": session.valueForKey("speedUnit") as String,
            "min_hot_temp": "\(minHotTemp)",
            "max_cold_temp": "\(maxColdTemp)",
            "genre": session.valueForKey("genre") as String
            ] as Dictionary
        
        let recommendation: ApiRecommendation? = request.getRecommendation(params)
        if recommendation?.statusCode == 200 {
            if let weather: APIWeather = recommendation?.weather as APIWeather! {
                if weather.overview != nil {
                    self.descriptionLabel.text = weather.overview!
                }
                if weather.temperatureValue != nil && weather.temperatureSymbol != nil {
                    self.temperatureLabel.text = "Temperature: \(weather.temperatureValue!)\(weather.temperatureSymbol!)"
                }
                if weather.precipitationValue != nil && weather.temperatureSymbol != nil {
                    self.precipitationLabel.text = "Precipitation chances: \(weather.precipitationValue!)\(weather.precipitationSymbol!)"
                }
                if weather.windValue != nil && weather.windSymbol != nil {
                    self.windLabel.text = "Wind speed: \(weather.windValue!)\(weather.windSymbol!)"
                }
                if weather.humidityValue != nil && weather.humiditySymbol != nil {
                    self.humidityLabel.text = "Humidity: \(weather.humidityValue!)\(weather.humiditySymbol!)"
                }
                if weather.sunrise != nil {
                    self.sunriseLabel.text = "Sunrise: \(weather.sunrise!)"
                }
                if weather.sunset != nil {
                    self.sunsetLabel.text = "Sunrise: \(weather.sunset!)"
                }
            }
            if let clothes: APIClothes = recommendation?.clothes as APIClothes! {
                if clothes.headLabel != nil && clothes.headImage != nil {
                    self.headLabel.text = clothes.headLabel!
                    self.headImage.image = request.getImageClothes(clothes.headImage!)
                }
                if clothes.neckLabel != nil && clothes.neckImage != nil {
                    self.neckLabel.text = clothes.neckLabel!
                    self.neckImage.image = request.getImageClothes(clothes.neckImage!)
                }
                if clothes.topLabel != nil && clothes.topImage != nil {
                    self.topLabel.text = clothes.topLabel!
                    self.topImage.image = request.getImageClothes(clothes.topImage!)
                }
                if clothes.legsLabel != nil && clothes.legsImage != nil {
                    self.legsLabel.text = clothes.legsLabel!
                    self.legsImage.image = request.getImageClothes(clothes.legsImage!)
                }
                if clothes.feetLabel != nil && clothes.feetImage != nil {
                    self.feetLabel.text = clothes.feetLabel!
                    self.feetImage.image = request.getImageClothes(clothes.feetImage!)
                }
            }
        } else {
            if recommendation?.errorMessage != nil {
                self.errorAlertView(recommendation?.errorMessage!)
            }
        }
    }
    
    func setDefaultSession() {
        var session:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if(session.valueForKey("username") == nil){
            titleLabel.topItem?.title = "Hello"
        } else {
            let username: String = session.valueForKey("username") as String
            titleLabel.topItem?.title = "Hello \(username)"
        }
        if(session.valueForKey("temperatureUnit") == nil) {
            session.setValue("c", forKey: "temperatureUnit")
            session.synchronize()
        }
        if(session.valueForKey("speedUnit") == nil) {
            session.setValue("m", forKey: "speedUnit")
            session.synchronize()
        }
        var temperatureUnit: String = session.valueForKey("temperatureUnit") as String
        if(session.valueForKey("minHotTemp") == nil) {
            if(temperatureUnit == "f") {
                session.setFloat(68, forKey: "minHotTemp")
            } else {
                session.setFloat(20, forKey: "minHotTemp")
            }
            session.synchronize()
        }
        if(session.valueForKey("maxColdTemp") == nil) {
            if(temperatureUnit == "f") {
                session.setFloat(50, forKey: "maxColdTemp")
            } else {
                session.setFloat(10, forKey: "maxColdTemp")
            }
            session.synchronize()
        }
        if(session.valueForKey("genre") == nil) {
            session.setValue("male", forKey: "genre")
            session.synchronize()
        }
    }
    
    func errorAlertView(errorMessage: String?) {
        var errorAlertView:UIAlertView = UIAlertView()
        errorAlertView.title = "Connection to API Failed!"
        errorAlertView.message = errorMessage
        errorAlertView.delegate = self
        errorAlertView.addButtonWithTitle("OK")
        errorAlertView.show()
    }
}

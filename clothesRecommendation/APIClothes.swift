import UIKit

class APIClothes: NSObject {
    
    private var _headLabel: String?
    var headLabel: String? {
        get {
            return self._headLabel
        }
        set {
            self._headLabel = newValue
        }
    }
    
    private var _headImage: String?
    var headImage: String? {
        get {
            return self._headImage
        }
        set {
            self._headImage = newValue
        }
    }
    
    private var _neckLabel: String?
    var neckLabel: String? {
        get {
            return self._neckLabel
        }
        set {
            self._neckLabel = newValue
        }
    }
    
    private var _neckImage: String?
    var neckImage: String? {
        get {
            return self._neckImage
        }
        set {
            self._neckImage = newValue
        }
    }
    
    private var _topLabel: String?
    var topLabel: String? {
        get {
            return self._topLabel
        }
        set {
            self._topLabel = newValue
        }
    }
    
    private var _topImage: String?
    var topImage: String? {
        get {
            return self._topImage
        }
        set {
            self._topImage = newValue
        }
    }
    
    private var _legsLabel: String?
    var legsLabel: String? {
        get {
            return self._legsLabel
        }
        set {
            self._legsLabel = newValue
        }
    }
    
    private var _legsImage: String?
    var legsImage: String? {
        get {
            return self._legsImage
        }
        set {
            self._legsImage = newValue
        }
    }
    
    private var _feetLabel: String?
    var feetLabel: String? {
        get {
            return self._feetLabel
        }
        set {
            self._feetLabel = newValue
        }
    }
    
    private var _feetImage: String?
    var feetImage: String? {
        get {
            return self._feetImage
        }
        set {
            self._feetImage = newValue
        }
    }
}

import Foundation

class Api {
    
    func connection(service: String, params: String, httpMethod: String) {
        var urlPath = "http://weather.generation-pc.net/"+service
        var url: NSURL = NSURL(string: urlPath)!
        
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        var params: String = params
        
        var session = NSURLSession.sharedSession()
        
        request.HTTPMethod = httpMethod
        var err: NSError?
        request.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        //request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
    }
}